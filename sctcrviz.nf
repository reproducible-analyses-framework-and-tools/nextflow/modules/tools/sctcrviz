process visualize_sctcr_outputs {

  tag "${dataset}/${pat_name}/${run}"
  label 'sctcrviz_container'
  label 'sctcrviz'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/sctcrviz"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(combinetcr), path(annotations)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path(combinetcr), path("*pdf"), emit: pdfs

  script:
  """
  sed -i 's/^${dataset}-${pat_name}-${run}_//g' ${combinetcr}

  echo "library(ggplot2)" > sctcr_viz.R
  echo "library(ggpattern)" >> sctcr_viz.R
  echo "library(ggtext)" >> sctcr_viz.R
  echo "library(dplyr)" >> sctcr_viz.R
  echo "library(tidyr)" >> sctcr_viz.R
  echo "clonotypes <- read.csv(\\"${combinetcr}\\", header=TRUE, sep='\\t')" >> sctcr_viz.R
  echo "fclonotypes <- clonotypes[,-1]" >> sctcr_viz.R
  echo "rownames(fclonotypes) <- clonotypes[,1]" >> sctcr_viz.R
  echo "annots <- read.csv(\\"${annotations}\\", header=TRUE, sep='\\t')" >> sctcr_viz.R
  echo "jdf <- merge(fclonotypes, annots, by=0)" >> sctcr_viz.R

  echo "write.table(jdf, file=\\"jdf_table.tsv\\", quote=FALSE, sep='\t')" >> sctcr_viz.R
#  echo "jdf %>% distinct(singler_annot)" >> sctcr_viz.R

  echo "jdf\\\$completeness <- ifelse(!(is.na(jdf\\\$cdr3_aa1)) & !(is.na(jdf\\\$cdr3_aa2)), \\"Complete\\", \\"Incomplete\\")" >> sctcr_viz.R
#  echo "jdf\\\$t_cell_annotation <- ifelse(grepl(\\"CD8\$\\", jdf\\\$singler_annot), \\"CD8+\\", \\"CD4+\\")" >> sctcr_viz.R
  echo "jdf\\\$t_cell_annotation <- jdf\\\$exp_annot" >> sctcr_viz.R

  echo "jdf_complete <- subset(jdf, completeness == 'Complete')" >> sctcr_viz.R

  echo "minjdf <- jdf %>% select('CTaa', 'completeness', 't_cell_annotation') %>% group_by(CTaa, t_cell_annotation) %>% mutate(CTaa=CTaa, completeness=completeness, occur=n(), freq=n()/nrow(jdf_complete)*100, t_cell_annotation=t_cell_annotation) %>% unique() %>% arrange(desc(freq)) %>% group_by(CTaa) %>% mutate(total_occur = sum(occur))" >> sctcr_viz.R

  echo "minjdfbytcell <- jdf %>% select('CTaa', 'completeness', 't_cell_annotation') %>% group_by(CTaa, t_cell_annotation) %>% mutate(CTaa=CTaa, completeness=completeness, occur=n(), freq=n()/nrow(jdf_complete)*100, t_cell_annotation=t_cell_annotation) %>% unique() %>% arrange(desc(freq)) %>% ungroup %>% mutate(tcr = row_number())" >> sctcr_viz.R

  echo "minjdfcomp <- jdf_complete %>% select('CTaa', 't_cell_annotation') %>% group_by(CTaa, t_cell_annotation) %>% mutate(CTaa=CTaa, occur=n(), freq=n()/nrow(jdf_complete)*100, t_cell_annotation=t_cell_annotation) %>% unique() %>% arrange(desc(freq)) %>% group_by(CTaa) %>% mutate(total_occur = sum(occur))" >> sctcr_viz.R

  echo "minjdfhead <- subset(minjdf, CTaa %in% head(unique(minjdf\\\$CTaa), n=10))" >> sctcr_viz.R
  echo "minjdfbytcellhead <- subset(minjdfbytcell, CTaa %in% head(unique(minjdf\\\$CTaa), n=10))" >> sctcr_viz.R
  echo "minjdfcomphead <- subset(minjdfcomp, CTaa %in% head(unique(minjdfcomp\\\$CTaa), n=10))" >> sctcr_viz.R
#  echo "minjdfhead <- minjdf" >> sctcr_viz.R

  echo "tcr_df <- c('total' = 'NA', 'total_cd8' = 'NA', 'complete_cd8' = 'NA', 'total_cd4' = 'NA', 'complete_cd4' = 'NA')" >> sctcr_viz.R

  echo "tcr_df[['total']] <- nrow(jdf)" >> sctcr_viz.R

  echo "jdf_cd8 <- subset(jdf, t_cell_annotation == \\"CD8+\\")" >> sctcr_viz.R
  echo "tcr_df[['total_cd8']] <- nrow(jdf_cd8)" >> sctcr_viz.R
  echo "jdf_cd8_complete <- subset(jdf_cd8, completeness == 'Complete')" >> sctcr_viz.R
  echo "tcr_df[['complete_cd8']] <- nrow(jdf_cd8_complete)" >> sctcr_viz.R

  echo "jdf_cd4 <- subset(jdf, t_cell_annotation == \\"CD4+\\")" >> sctcr_viz.R
  echo "tcr_df[['total_cd4']] <- nrow(jdf_cd4)" >> sctcr_viz.R
  echo "jdf_cd4_complete <- subset(jdf_cd4, completeness == 'Complete')" >> sctcr_viz.R
  echo "tcr_df[['complete_cd4']] <- nrow(jdf_cd4_complete)" >> sctcr_viz.R

  echo "tcr_df" >> sctcr_viz.R

  echo "p <- ggplot(minjdfhead, aes(x=reorder(CTaa, -freq, sum), y=freq, fill=t_cell_annotation, pattern=completeness)) + scale_fill_manual(values = c('CD4+' = 'darksalmon', 'CD8+' = 'lightblue'), name='T Cell\nAnnotation', guide=guide_legend(override.aes = list(pattern = 'none'))) + geom_bar_pattern(pattern_fill = 'black', pattern_angle = 45, pattern_density = 0.1, pattern_spacing = 0.025, pattern_key_scale_factor = 0.6, stat=\\"identity\\") + scale_pattern_manual(guide=guide_legend(override.aes = list(pattern_fill = 'seashell')), values = c(\\"Incomplete\\" = 'stripe', 'Complete' = \\"none\\"), name='TCR\nCompleteness') + theme_minimal() + theme(axis.text.x = element_text(angle = 45, vjust = 1, hjust=1)) + xlab(\\"TCR AA Sequences\\") + ylab(\\"Frequency (%)\\") + ggtitle(\\"Top 10 Most Abundant TCRs\n${pat_name}\\") + geom_richtext(data = NULL, fill = 'white', hjust=\\"left\\", x = 4, y = max(minjdfhead\\\$freq)-(0.15*max(minjdfhead\\\$freq)), label = paste(\\"Total number of transcriptomes:\\", tcr_df[['total']], \\"<br>**CD8+** T cells total count:\\",  tcr_df[['total_cd8']], \\"<br>**CD8+** T cells (called TCRs) count:\\", tcr_df[['complete_cd8']], \\"<br>**CD4+** T cells total count:\\", tcr_df[['total_cd4']], \\"<br>**CD4+** T cells (called TCRs) count:\\", tcr_df[['complete_cd4']])) + theme(plot.title = element_text(hjust = 0.5)) + scale_x_discrete(labels= c('TCR1', 'TCR2', 'TCR3', 'TCR4', 'TCR5', 'TCR6', 'TCR7', 'TCR8', 'TCR9', 'TCR10'))" >> sctcr_viz.R

  echo "p1 <- ggplot(minjdfhead, aes(x=reorder(CTaa, -freq, sum), y=freq, fill=t_cell_annotation, pattern=completeness)) + scale_fill_manual(values = c('CD4+' = 'darksalmon', 'CD8+' = 'lightblue'), name='T Cell\nAnnotation', guide=guide_legend(override.aes = list(pattern = 'none'))) + geom_bar_pattern(pattern_fill = 'black', pattern_angle = 45, pattern_density = 0.1, pattern_spacing = 0.025, pattern_key_scale_factor = 0.6, stat=\\"identity\\") + scale_pattern_manual(guide=guide_legend(override.aes = list(pattern_fill = 'seashell')), values = c(\\"Incomplete\\" = 'stripe', 'Complete' = \\"none\\"), name='TCR\nCompleteness') + theme_minimal() + theme(axis.text.x = element_text(angle = 45, vjust = 1, hjust=1)) + xlab(\\"TCR AA Sequences\\") + ylab(\\"Frequency (%)\\") + ggtitle(\\"Top 10 Most Abundant TCRs\n${pat_name}\\") + geom_richtext(data = NULL, fill = 'white', hjust=\\"left\\", x = 4, y = max(minjdfhead\\\$freq)-(0.15*max(minjdfhead\\\$freq)), label = paste(\\"Total number of transcriptomes:\\", tcr_df[['total']], \\"<br>**CD8+** T cells total count:\\",  tcr_df[['total_cd8']], \\"<br>**CD8+** T cells (called TCRs) count:\\", tcr_df[['complete_cd8']], \\"<br>**CD4+** T cells total count:\\", tcr_df[['total_cd4']], \\"<br>**CD4+** T cells (called TCRs) count:\\", tcr_df[['complete_cd4']])) + theme(plot.title = element_text(hjust = 0.5)) + scale_x_discrete(labels= c('TCR1', 'TCR2', 'TCR3', 'TCR4', 'TCR5', 'TCR6', 'TCR7', 'TCR8', 'TCR9', 'TCR10'))" >> sctcr_viz.R

  echo "p2 <- ggplot(minjdfbytcellhead, aes(x=reorder(tcr, -freq, sum), y=freq, fill=t_cell_annotation, pattern=completeness)) + scale_fill_manual(values = c('CD4+' = 'darksalmon', 'CD8+' = 'lightblue'), name='T Cell\nAnnotation', guide=guide_legend(override.aes = list(pattern = 'none'))) + geom_bar_pattern(pattern_fill = 'black', pattern_angle = 45, pattern_density = 0.1, pattern_spacing = 0.025, pattern_key_scale_factor = 0.6, stat=\\"identity\\") + scale_pattern_manual(guide=guide_legend(override.aes = list(pattern_fill = 'seashell')), values = c(\\"Incomplete\\" = 'stripe', 'Complete' = \\"none\\"), name='TCR\nCompleteness') + theme_minimal() + theme(axis.text.x = element_text(angle = 45, vjust = 1, hjust=1)) + xlab(\\"TCR AA Sequences\\") + ylab(\\"Frequency (%)\\") + ggtitle(\\"Top 10 Most Abundant TCRs\n${pat_name}\\") + geom_richtext(data = NULL, fill = 'white', hjust=\\"left\\", x = 4, y = max(minjdfhead\\\$freq)-(0.15*max(minjdfhead\\\$freq)), label = paste(\\"Total number of transcriptomes:\\", tcr_df[['total']], \\"<br>**CD8+** T cells total count:\\",  tcr_df[['total_cd8']], \\"<br>**CD8+** T cells (called TCRs) count:\\", tcr_df[['complete_cd8']], \\"<br>**CD4+** T cells total count:\\", tcr_df[['total_cd4']], \\"<br>**CD4+** T cells (called TCRs) count:\\", tcr_df[['complete_cd4']])) + theme(plot.title = element_text(hjust = 0.5)) + scale_x_discrete(labels= c('TCR1', 'TCR2', 'TCR3', 'TCR4', 'TCR5', 'TCR6', 'TCR7', 'TCR8', 'TCR9', 'TCR10'))" >> sctcr_viz.R

  echo "ggsave(\\"${dataset}-${pat_name}-${run}.pdf\\", p)" >> sctcr_viz.R
  echo "ggsave(\\"${dataset}-${pat_name}-${run}.complete_tcrs.pdf\\", p1)" >> sctcr_viz.R
  echo "ggsave(\\"${dataset}-${pat_name}-${run}.by_t_cell_annot.pdf\\", p2)" >> sctcr_viz.R

  echo "write.table(minjdfhead, file=\\"jdf_table_final.top_10_abundant_tcrs.tsv\\", quote=FALSE, sep='\t')" >> sctcr_viz.R
  echo "write.table(minjdf, file=\\"jdf_table_final.all_tcrs.tsv\\", quote=FALSE, sep='\t')" >> sctcr_viz.R


  Rscript sctcr_viz.R
  """
}
